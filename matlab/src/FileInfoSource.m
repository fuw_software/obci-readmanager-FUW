% Copyright (c) 2016-2018 Braintech Sp. z o.o. [Ltd.] <http://www.braintech.pl>
% All rights reserved.

class FileInfoSource
    properties
    filename
    end
    methods
    function obj = FileInfoSource(filename)
    obj.filename=filename;
    end
end