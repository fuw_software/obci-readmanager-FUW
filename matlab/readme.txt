Poniższe działa na MATLAB R2017a, wersja trial bez żadnych dodatków.

1. Wygenerowanie pakietu (toolboxa) matlabowego z plików:

Trzeba wyklikać:
https://www.mathworks.com/help/matlab/matlab_prog/create-and-share-custom-matlab-toolboxes.html#bugxfu9
(wystarczy dodać pliki z katalogu src, wpisać nazwę toolboxa(obci_readmanager) i kliknąć package.
Pojawi się plik obci_readmanager.mltbx.


2. Instalacja toolboxa:

Wpisujemy w matlabie: matlab.addons.toolbox.installToolbox('obci_readmanager.mltbx')


3. Przykładowe użycie:

Wpisujemy w matlabie:
TestTags (w okienku Workspace pojawiają się różne obiekty)

albo wpisujemy:

x = ReadManager('test_data.obci.info', 'test_data.obci.dat', 'test_data.obci.tags')
x.get_samples
x.get_start_timestamp
x.get_params
