#!/usr/bin/env python3
# Copyright (c) 2016-2018 Braintech Sp. z o.o. [Ltd.] <http://www.braintech.pl>
# All rights reserved.


import time

import numpy

from obci_readmanager.signal_processing.buffers import auto_blink_buffer as R
from obci_readmanager.signal_processing.signal.data_generic_write_proxy import SamplePacket

try:
    from .test_auto_ring_buffer import ArrayCallback
except (SystemError, ImportError):
    from test_auto_ring_buffer import ArrayCallback


class BlinkArrayCallback(ArrayCallback):
    def __call__(self, blink, arr):
        super(BlinkArrayCallback, self).__call__(arr.copy())


def test_auto_blink_buffer():
    per, ch, sm = (4, 3, 20)

    ts_step = 1 / float(sm)
    callback = BlinkArrayCallback()

    b = R.AutoBlinkBuffer(0, 5, ch, sm, callback, False)

    t = time.time()

    ts = numpy.linspace(t, t + 200 * ts_step, 200)

    b.handle_sample_packet(get_sample_packet(per, ch, ts[0 * per], ts_step))  # 1-4

    b.handle_blink(B(ts[1 * per]))
    # AutoBlinkBuffer - Got blink before buffer is full. Ignore!

    b.handle_sample_packet(get_sample_packet(per, ch, ts[1 * per + 1], ts_step))  # 5-8

    b.handle_sample_packet(get_sample_packet(per, ch, ts[2 * per + 1], ts_step))  # 9-12

    b.handle_sample_packet(get_sample_packet(per, ch, ts[3 * per + 1], ts_step))  # 13-16

    b.handle_blink(B(ts[4 * per + 1]))

    b.handle_sample_packet(get_sample_packet(per, ch, ts[4 * per + 2], ts_step))  # 17-20

    b.handle_sample_packet(get_sample_packet(per, ch, ts[5 * per + 2], ts_step))  # 21-24
    callback.assert_called([[18., 19., 20., 21., 22.],
                            [180., 190., 200., 210., 220.],
                            [1800., 1900., 2000., 2100., 2200.]])

    b.handle_blink(B(ts[6 * per + 2]))

    b.handle_sample_packet(get_sample_packet(per, ch, ts[6 * per + 3], ts_step))  # 25-28

    b.handle_sample_packet(get_sample_packet(per, ch, ts[7 * per + 3], ts_step))  # 29-32
    callback.assert_called([[26., 27., 28., 29., 30.],
                            [260., 270., 280., 290., 300.],
                            [2600., 2700., 2800., 2900., 3000.]])

    b.handle_blink(B(ts[8 * per + 3]))

    b.handle_sample_packet(get_sample_packet(per, ch, ts[8 * per + 4], ts_step))  # 33-36

    b.handle_sample_packet(get_sample_packet(per, ch, ts[9 * per + 4], ts_step))  # 37-40
    callback.assert_called(
        [[34., 35., 36., 37., 38.],
         [340., 350., 360., 370., 380.],
         [3400., 3500., 3600., 3700., 3800.]])

    zero_count()

    b = R.AutoBlinkBuffer(0, 10, ch, sm, callback, False)

    b.handle_sample_packet(get_sample_packet(per, ch, ts[10 * per + 4], ts_step))  # 1-4

    b.handle_blink(B(ts[11 * per + 4]))
    # AutoBlinkBuffer - Got blink before buffer is full. Ignore!

    b.handle_sample_packet(get_sample_packet(per, ch, ts[11 * per + 5], ts_step))

    b.handle_sample_packet(get_sample_packet(per, ch, ts[12 * per + 5], ts_step))

    b.handle_sample_packet(get_sample_packet(per, ch, ts[13 * per + 5], ts_step))

    b.handle_sample_packet(get_sample_packet(per, ch, ts[14 * per + 5], ts_step))  # 17-20

    b.handle_sample_packet(get_sample_packet(per, ch, ts[15 * per + 5], ts_step))

    b.handle_blink(B(ts[16 * per + 5]))

    b.handle_sample_packet(get_sample_packet(per, ch, ts[16 * per + 6], ts_step))  # 25-28

    b.handle_sample_packet(get_sample_packet(per, ch, ts[17 * per + 6], ts_step))

    b.handle_blink(B(ts[18 * per + 6]))

    b.handle_sample_packet(get_sample_packet(per, ch, ts[18 * per + 7], ts_step))  # 33-36
    callback.assert_called(
        [[26., 27., 28., 29., 30., 31., 32., 33., 34., 35.],
         [260., 270., 280., 290., 300., 310., 320., 330., 340., 350.],
         [2600., 2700., 2800., 2900., 3000., 3100., 3200., 3300., 3400., 3500.]])

    b.handle_blink(B(ts[19 * per + 7]))

    b.handle_sample_packet(get_sample_packet(per, ch, ts[19 * per + 8], ts_step))

    b.handle_sample_packet(get_sample_packet(per, ch, ts[20 * per + 8], ts_step))

    callback.assert_called(
        [[34., 35., 36., 37., 38., 39., 40., 41., 42., 43.],
         [340., 350., 360., 370., 380., 390., 400., 410., 420., 430.],
         [3400., 3500., 3600., 3700., 3800., 3900., 4000., 4100., 4200., 4300.]])

    b.handle_sample_packet(get_sample_packet(per, ch, ts[21 * per + 8], ts_step))
    callback.assert_called(
        [[38., 39., 40., 41., 42., 43., 44., 45., 46., 47.],
         [380., 390., 400., 410., 420., 430., 440., 450., 460., 470.],
         [3800., 3900., 4000., 4100., 4200., 4300., 4400., 4500., 4600., 4700.]])

    b.handle_sample_packet(get_sample_packet(per, ch, ts[22 * per + 8], ts_step))

    zero_count()

    b = R.AutoBlinkBuffer(0, 10, ch, sm, callback, False)

    b.handle_sample_packet(get_sample_packet(per, ch, ts[23 * per + 8], ts_step))  # 1-4

    b.handle_sample_packet(get_sample_packet(per, ch, ts[24 * per + 8], ts_step))

    b.handle_sample_packet(get_sample_packet(per, ch, ts[25 * per + 8], ts_step))

    b.handle_sample_packet(get_sample_packet(per, ch, ts[26 * per + 8], ts_step))

    b.handle_sample_packet(get_sample_packet(per, ch, ts[27 * per + 8], ts_step))  # 17-20

    b.handle_sample_packet(get_sample_packet(per, ch, ts[28 * per + 8], ts_step))

    b.handle_blink(B(ts[29 * per + 2]))

    b.handle_blink(B(ts[29 * per + 3]))

    b.handle_blink(B(ts[29 * per + 4]))

    b.handle_blink(B(ts[29 * per + 5]))

    b.handle_blink(B(ts[29 * per + 6]))

    b.handle_blink(B(ts[29 * per + 7]))

    b.handle_blink(B(ts[29 * per + 8]))

    b.handle_sample_packet(get_sample_packet(per, ch, ts[29 * per + 10], ts_step))  # 25-28
    callback.assert_called(
        [[19., 20., 21., 22., 23., 24., 25., 26., 27., 28.],
         [190., 200., 210., 220., 230., 240., 250., 260., 270., 280.],
         [1900., 2000., 2100., 2200., 2300., 2400., 2500., 2600., 2700., 2800.]])

    b.handle_sample_packet(get_sample_packet(per, ch, ts[30 * per + 10], ts_step))
    callback.assert_called(
        [[20., 21., 22., 23., 24., 25., 26., 27., 28., 29.],
         [200., 210., 220., 230., 240., 250., 260., 270., 280., 290.],
         [2000., 2100., 2200., 2300., 2400., 2500., 2600., 2700., 2800., 2900.]])
    callback.assert_called(
        [[21., 22., 23., 24., 25., 26., 27., 28., 29., 30.],
         [210., 220., 230., 240., 250., 260., 270., 280., 290., 300.],
         [2100., 2200., 2300., 2400., 2500., 2600., 2700., 2800., 2900., 3000.]])
    callback.assert_called(
        [[22., 23., 24., 25., 26., 27., 28., 29., 30., 31.],
         [220., 230., 240., 250., 260., 270., 280., 290., 300., 310.],
         [2200., 2300., 2400., 2500., 2600., 2700., 2800., 2900., 3000., 3100.]])
    callback.assert_called(
        [[23., 24., 25., 26., 27., 28., 29., 30., 31., 32.],
         [230., 240., 250., 260., 270., 280., 290., 300., 310., 320.],
         [2300., 2400., 2500., 2600., 2700., 2800., 2900., 3000., 3100., 3200.]])

    b.handle_sample_packet(get_sample_packet(per, ch, ts[31 * per + 10], ts_step))  # 33-36
    callback.assert_called(
        [[25., 26., 27., 28., 29., 30., 31., 32., 33., 34.],
         [250., 260., 270., 280., 290., 300., 310., 320., 330., 340.],
         [2500., 2600., 2700., 2800., 2900., 3000., 3100., 3200., 3300., 3400.]])
    callback.assert_called(
        [[26., 27., 28., 29., 30., 31., 32., 33., 34., 35.],
         [260., 270., 280., 290., 300., 310., 320., 330., 340., 350.],
         [2600., 2700., 2800., 2900., 3000., 3100., 3200., 3300., 3400., 3500.]])

    b.handle_sample_packet(get_sample_packet(per, ch, ts[32 * per + 10], ts_step))  # 33-36

    b.handle_blink(B(ts[33 * per + 10]))

    b.handle_sample_packet(get_sample_packet(per, ch, ts[33 * per + 11], ts_step))

    b.handle_blink(B(ts[34 * per + 11]))

    b.handle_sample_packet(get_sample_packet(per, ch, ts[34 * per + 12], ts_step))  # 41-44

    b.handle_sample_packet(get_sample_packet(per, ch, ts[35 * per + 12], ts_step))
    callback.assert_called(
        [[42., 43., 44., 45., 46., 47., 48., 49., 50., 51.],
         [420., 430., 440., 450., 460., 470., 480., 490., 500., 510.],
         [4200., 4300., 4400., 4500., 4600., 4700., 4800., 4900., 5000., 5100.]])

    b.handle_sample_packet(get_sample_packet(per, ch, ts[36 * per + 12], ts_step))
    callback.assert_called(
        [[46., 47., 48., 49., 50., 51., 52., 53., 54., 55.],
         [460., 470., 480., 490., 500., 510., 520., 530., 540., 550.],
         [4600., 4700., 4800., 4900., 5000., 5100., 5200., 5300., 5400., 5500.]])


COUNT = 0


class B(object):
    def __init__(self, t):
        self.timestamp = t


def zero_count():
    global COUNT
    COUNT = 0


def get_sample_packet(per, ch_num, ts_start, ts_step):
    """

    :param per: number of samples
    :param ch_num: number of channels
    :param ts_step: time step for generating samples
    :param ts_start:
    :return: signal as tuple with two numpy arrays
    (numpy.array( [ 1485342990.17, 1485342990.28, 1485342990.42, ...]),
     numpy.array([
                    [ 1, 10, 100, ...],
                    [ 2, 20, 200, ...],
                    [ 3, 30, 300, ...],
                    [ 4, 400, 400, ...]
                ])
    )
    """

    global COUNT
    samples = numpy.zeros([per, ch_num])
    timestamps = numpy.zeros(per)
    for i in range(per):
        COUNT += 1
        for j in range(ch_num):
            samples[i][j] = (10 ** j) * COUNT
        timestamps[i] = ts_start + ts_step * i
    return SamplePacket(samples, timestamps)
