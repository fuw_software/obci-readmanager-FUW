#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (c) 2016-2018 Braintech Sp. z o.o. [Ltd.] <http://www.braintech.pl>
# All rights reserved.
import numpy as np

from obci_readmanager.signal_processing.buffers.ring_buffer import RingBuffer
from obci_readmanager.signal_processing.signal.data_raw_write_proxy import SamplePacket


def test_ring_buffer():
    r = RingBuffer(10, 5, False)

    for s in [get_sample(i, 5) for i in range(12)]:
        r.add(s)

    def assert_equal(arr1, arr2):
        assert np.array_equal(np.array(arr1), np.array(arr2))

    vals = r.get(3, 3)

    assert_equal(vals,
                 [[5.00000000e+00, 6.00000000e+00, 7.00000000e+00],
                  [5.00000000e+01, 6.00000000e+01, 7.00000000e+01],
                  [5.00000000e+02, 6.00000000e+02, 7.00000000e+02],
                  [5.00000000e+03, 6.00000000e+03, 7.00000000e+03],
                  [5.00000000e+04, 6.00000000e+04, 7.00000000e+04]])

    vals[2, 1] = 1234.0

    assert_equal(vals, [[5.00000000e+00, 6.00000000e+00, 7.00000000e+00],
                        [5.00000000e+01, 6.00000000e+01, 7.00000000e+01],
                        [5.00000000e+02, 1.23400000e+03, 7.00000000e+02],
                        [5.00000000e+03, 6.00000000e+03, 7.00000000e+03],
                        [5.00000000e+04, 6.00000000e+04, 7.00000000e+04]])

    assert_equal(r.get(3, 3), [[5.00000000e+00, 6.00000000e+00, 7.00000000e+00],
                               [5.00000000e+01, 6.00000000e+01, 7.00000000e+01],
                               [5.00000000e+02, 1.23400000e+03, 7.00000000e+02],
                               [5.00000000e+03, 6.00000000e+03, 7.00000000e+03],
                               [5.00000000e+04, 6.00000000e+04, 7.00000000e+04]])

    r = RingBuffer(10, 5, True)

    for s in [get_sample(i, 5) for i in range(19)]:
        r.add(s)

    vals = r.get(2, 2)

    assert_equal(vals, [[1.10000000e+01, 1.20000000e+01],
                        [1.10000000e+02, 1.20000000e+02],
                        [1.10000000e+03, 1.20000000e+03],
                        [1.10000000e+04, 1.20000000e+04],
                        [1.10000000e+05, 1.20000000e+05]])


def get_sample(v, ch_num, per=1):
    """

    :param v: multiplication factor
    :param ch_num: number of channels
    :return: one sample as numpy array
     numpy.array( [ 1, 10, 100, ...] )
    """
    sample = np.zeros([per, ch_num])
    timestamp = np.zeros(per)

    mult = 1
    for i in range(ch_num):
        sample[0][i] = float(v) * mult
        mult *= 10
    timestamp[0] = 10.0
    return SamplePacket(sample, timestamp).samples[0]
