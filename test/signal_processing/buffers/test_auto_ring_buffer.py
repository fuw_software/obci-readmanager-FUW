#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (c) 2016-2018 Braintech Sp. z o.o. [Ltd.] <http://www.braintech.pl>
# All rights reserved.
from unittest.mock import Mock

import numpy
from obci_readmanager.signal_processing.signal.data_raw_write_proxy import SamplePacket


class ArrayCallback(Mock):
    def __init__(self, *args, **kwargs):
        super(ArrayCallback, self).__init__(*args, **kwargs)
        self.current_check = 0

    def assert_called(self, arr2):
        assert self.current_check < self.call_count
        arr1 = numpy.array(self.call_args_list[self.current_check][0][0])
        arr2 = numpy.array(arr2)
        assert numpy.array_equal(arr1, arr2)
        self.current_check += 1

    def __call__(self, arr):
        super(ArrayCallback, self).__call__(arr.copy())


def test_auto_ring_buffer():
    from obci_readmanager.signal_processing.buffers import auto_ring_buffer as R

    per, ch = (4, 10)
    callback = ArrayCallback()

    b = R.AutoRingBuffer(10, 5, 5, ch, callback, False)

    v1 = get_sample_packet(per, ch, 1)

    v2 = get_sample_packet(per, ch, 10)

    v3 = get_sample_packet(per, ch, 100)

    v4 = get_sample_packet(per, ch, 1000)

    b.handle_sample_packet(v1)

    b.handle_sample_packet(v2)

    b.handle_sample_packet(v3)
    callback.assert_called(
        [[1., 2., 3., 4., 10.],
         [2., 3., 4., 5., 20.],
         [3., 4., 5., 6., 30.],
         [4., 5., 6., 7., 40.],
         [5., 6., 7., 8., 50.],
         [6., 7., 8., 9., 60.],
         [7., 8., 9., 10., 70.],
         [8., 9., 10., 11., 80.],
         [9., 10., 11., 12., 90.],
         [10., 11., 12., 13., 100.]])

    b.handle_sample_packet(v4)
    callback.assert_called(
        [[11., 12., 13., 100., 101.],
         [21., 22., 23., 200., 201.],
         [31., 32., 33., 300., 301.],
         [41., 42., 43., 400., 401.],
         [51., 52., 53., 500., 501.],
         [61., 62., 63., 600., 601.],
         [71., 72., 73., 700., 701.],
         [81., 82., 83., 800., 801.],
         [91., 92., 93., 900., 901.],
         [101., 102., 103., 1000., 1001.]])

    b = R.AutoRingBuffer(10, 3, 5, ch, callback, False)

    b.handle_sample_packet(v1)

    b.handle_sample_packet(v2)

    b.handle_sample_packet(v3)
    callback.assert_called(
        [[1., 2., 3.],
         [2., 3., 4.],
         [3., 4., 5.],
         [4., 5., 6.],
         [5., 6., 7.],
         [6., 7., 8.],
         [7., 8., 9.],
         [8., 9., 10.],
         [9., 10., 11.],
         [10., 11., 12.]])

    b.handle_sample_packet(v4)
    callback.assert_called(
        [[11., 12., 13.],
         [21., 22., 23.],
         [31., 32., 33.],
         [41., 42., 43.],
         [51., 52., 53.],
         [61., 62., 63.],
         [71., 72., 73.],
         [81., 82., 83.],
         [91., 92., 93.],
         [101., 102., 103.]])


def get_sample_packet(per, ch, mult):
    """

    :param per: number of samples
    :param ch: number of channels
    :param mult: multiplication parameter
    :return: signal as tuple with two numpy arrays
    (numpy.array( [ 1485342990.17, 1485342990.28, 1485342990.42, ...]),
     numpy.array([
                    [ 1, 10, 100, ...],
                    [ 2, 20, 200, ...],
                    [ 3, 30, 300, ...],
                    [ 4, 400, 400, ...]
                ])
    )
    """

    timestamps = numpy.zeros(per)
    packet = numpy.zeros([per, ch])
    for i in range(per):
        for j in range(ch):
            packet[i][j] = float(j + 1) * mult + i
        timestamps[i] = 10.0
    return SamplePacket(packet, timestamps)


def get_sample_packet(per, ch, mult):
    """

    :param per: number of samples
    :param ch: number of channels
    :param mult: multiplication parameter
    :return: signal as tuple with two numpy arrays
    (numpy.array( [ 1485342990.17, 1485342990.28, 1485342990.42, ...]),
     numpy.array([
                    [ 1, 10, 100, ...],
                    [ 2, 20, 200, ...],
                    [ 3, 30, 300, ...],
                    [ 4, 400, 400, ...]
                ])
    )
    """

    timestamps = numpy.zeros(per)
    packet = numpy.zeros([per, ch])
    for i in range(per):
        for j in range(ch):
            packet[i][j] = float(j + 1) * mult + i
        timestamps[i] = 10.0
    return SamplePacket(packet, timestamps)

