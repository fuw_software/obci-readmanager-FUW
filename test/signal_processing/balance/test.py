#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (c) 2016-2018 Braintech Sp. z o.o. [Ltd.] <http://www.braintech.pl>
# All rights reserved.

from __future__ import print_function, division
from obci_readmanager.signal_processing.balance.wii_preprocessing import wii_filter_signal, wii_downsample_signal, wii_cut_fragments
from obci_readmanager.signal_processing.balance.wii_analysis import wii_COP_path
from obci_readmanager.signal_processing.balance.wii_read_manager import WBBReadManager
import matplotlib.pyplot as py


def test_wii_analysis(plot=False):
    w = WBBReadManager('test1.obci.xml', 'test1.obci.raw', 'test1.obci.tag')
    w.get_x()
    w.get_y()
    wbb_mgr = wii_filter_signal(w, 30.0, 2, use_filtfilt=False)
    wbb_mgr = wii_downsample_signal(wbb_mgr, factor=2, pre_filter=True, use_filtfilt=True)
    smart_tags = wii_cut_fragments(wbb_mgr)
    for sm in smart_tags:
        sm_x = sm.get_channel_samples('x')
        sm_y = sm.get_channel_samples('y')
        py.figure()
        print(wii_COP_path(wbb_mgr, sm_x, sm_y, plot=True))
    if plot:
        py.show()


if __name__ == '__main__':
    test_wii_analysis(plot=True)
